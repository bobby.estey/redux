// create store, create and combine reducers from redux
import {createStore, combineReducers, applyMiddleware} from 'redux';

// logs to the console (Development Tools)
import {createLogger} from 'redux-logger';

import {counterReducer} from './models/counter/CounterReducer';

const logger = createLogger();

// combine / encapsulate all the reducers under a root reducer
// export const rootReducer = combineReducers({counterReducer, anotherReducer, yetAnotherReducer});
export const rootReducer = combineReducers({counterReducer});

// AppState has the (state and type) being returned from reducers
export type AppState = ReturnType<typeof rootReducer>;

// creates the store, Store - Contains the State and sends to the Component
export const store = createStore(rootReducer, applyMiddleware(logger));
