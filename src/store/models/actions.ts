// mapping to the various actions
import {CounterActionTypes} from '../models/counter/models/actions';

export type AppActions = CounterActionTypes; // | TodoActionTypes
