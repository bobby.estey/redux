// these are actions to be dispatched, action types
export const INCREMENT_COUNTER = 'INCREMENT_COUNTER';
export const DECREMENT_COUNTER = 'DECREMENT_COUNTER';

interface IncrementAction {
    type: typeof INCREMENT_COUNTER;
}

interface DecrementAction {
    type: typeof DECREMENT_COUNTER;
}

// global action types which contains all the action types
export type CounterActionTypes = IncrementAction | DecrementAction;
