// interface Counter - force a data structure on an Object
export interface Counter {
    count: number;
}
