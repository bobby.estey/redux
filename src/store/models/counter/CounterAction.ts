import {INCREMENT_COUNTER, DECREMENT_COUNTER} from './models/actions';

// the action to trigger the functionality increment count
export const incrementCount = () => ({
    type: INCREMENT_COUNTER,
});

// the action to trigger the functionality decrement count
export const decrementCount = () => ({
    type: DECREMENT_COUNTER,
});
