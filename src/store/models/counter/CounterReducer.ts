// import the action types
import {
    CounterActionTypes,
    INCREMENT_COUNTER,
    DECREMENT_COUNTER,
} from './models/actions';

// import the Counter model
import {Counter} from './models/Counter';

// import the reducer from redux
import {Action, Reducer} from 'redux';

// 
const defaultState: Counter = {
    count: 0,
};

// create the reducer being mapped to the action - the reducer is a pure function
export const counterReducer: Reducer<Counter, Action> = (
    state = defaultState,
    action: CounterActionTypes
) => {
    const nextState = {
        count: state.count,
    };
    // how to handle the various types of actions
    switch (action.type) {
        case INCREMENT_COUNTER:
            nextState.count = state.count + 1;
            return nextState;
        case DECREMENT_COUNTER:
            nextState.count = state.count - 1;
            return nextState;
        default:
            return state;
    }
};
