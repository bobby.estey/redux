import React, {Component} from 'react';

// component needs to subscribes to the store - instead of a subscription through other means
import {connect} from 'react-redux';

// actions
import {Dispatch} from 'redux';

// AppState has the (state and type) being returned from reducers
import {AppState} from './store/rootStore';

// dispatch actions through props
import {incrementCount, decrementCount} from './store/models/counter/CounterAction';

import HomePage from './components/HomePage';

interface AppProps {
    increment: () => void;
    decrement: () => void;
}

// provide the current state through properties
const mapStateToProps = (state: AppState) => ({
    count: state.counterReducer.count,
});

// dispatch our action through properties
// if the increment button is pressed then the increment case, will call increment:
const mapDispatchToProps = (dispatch: Dispatch): AppProps => ({
    increment: () => dispatch(incrementCount()),
    decrement: () => dispatch(decrementCount()),
});

// Component has AppProps - comes from mapDispatchToProps and AppState - comes from mapStateToProps
class App extends Component<AppProps, AppState> {
    render(): JSX.Element {

        // destructure this.props
        return <HomePage {...this.props} />;
    }
}

// higher order function, calling another function, which are located above
export default connect(mapStateToProps, mapDispatchToProps)(App);
